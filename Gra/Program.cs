﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraWZycie
{

    class Program
    {
        static class stale
        {
            public const int rozmiar = 10;
        }

        public class komorka
        {
            public int stan;
            public int ile_zywych_sasiadow;

            public komorka() { stan = 0; }

            public komorka(int a)
            {
                if (a == 1)
                    stan = 1;
                else
                    stan = 0;
            }

            public int czy_osamotniona()
            {
                if (ile_zywych_sasiadow < 2)
                    return 0;
                else return 1;
            }

            public int czy_przeludnienie()
            {
                if (ile_zywych_sasiadow > 3)
                    return 0;
                else return 1;
            }

            public int czy_zmarwtychwstanie()
            {
                if (ile_zywych_sasiadow == 3)
                    return 1;
                else return 0;
            }
        }

        public class swiat
        {

            public komorka[,] tab = new komorka[stale.rozmiar + 2, stale.rozmiar + 2];
            public Random r1 = new Random();

            public swiat()
            {
                for (int i = 0; i < stale.rozmiar + 2; i++)
                    for (int j = 0; j < stale.rozmiar + 2; j++)
                        tab[i, j] = new komorka(-1);

                for (int i = 1; i < stale.rozmiar + 1; i++)
                    for (int j = 1; j < stale.rozmiar + 1; j++)
                        tab[i, j] = new komorka(r1.Next(0, 2));
            }

            public int licz(int i, int j)
            {
                int ile = 0;

                for (int k = i - 1; k <= i + 1; k++)
                    for (int l = j - 1; l <= j + 1; l++)
                        if (k != i && l != j)
                            if (tab[k, l].stan == 1)
                                ile++;

                return ile;
            }

            public void wyswietl()
            {
                for (int i = 1; i < 11; i++)
                {
                    for (int j = 1; j < 11; j++)
                    {
                        Console.Write(tab[i, j].stan);
                    }
                    Console.WriteLine();
                }
            }
        }

        static void Main(string[] args)
        {
            ConsoleKeyInfo cki;
            swiat zycie = new swiat();
            swiat kopia = new swiat();
            zycie.wyswietl();
            Console.WriteLine();
            while (true)
            {
                cki = Console.ReadKey(true);
                if (cki.KeyChar == 13)
                {
                    kopia = zycie;
                    for (int i = 1; i < stale.rozmiar + 1; i++)
                    {
                        for (int j = 1; j < stale.rozmiar + 1; j++)
                        {
                            zycie.tab[i, j].ile_zywych_sasiadow = zycie.licz(i, j);
                            if (zycie.tab[i, j].stan == 1 && (zycie.tab[i, j].czy_osamotniona() == 0 || zycie.tab[i, j].czy_przeludnienie() == 0)) kopia.tab[i, j].stan = 0;
                            if (zycie.tab[i, j].stan == 0 && zycie.tab[i, j].czy_zmarwtychwstanie() == 1) kopia.tab[i, j].stan = 1;
                        }
                    }
                    zycie = kopia;
                    Console.Clear();
                    zycie.wyswietl();
                    Console.WriteLine();
                    
                }
            }
            Console.ReadKey();
            
        }
    }

}